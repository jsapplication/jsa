# jsApplication (jsa) Runtime

This is the run-time library of [jsApplication](https://jsapplication.gitlab.io/jsapplication/). Use this to include jsApplication in your website. Please find the documentation and examples on the jsa website and main repository as linked below.

## jsApplication: Desktop-like applications in JavaScript

jsApplication is a JavaScript framework to build desktop-like applications as single-page, client-only HTML pages. It provides classes for UI elements that are to be used as UI programming in object-oriented languages as Java or C#. jsApplication provides standard UI elements as View, Containers, Menu, Button, Table, MsgBox, Modal, Tabbar, Splash, TextCtrl, SelectCtrl, RadioCtrl, CheckboxCtrl. In addition, it includes unorthodox UI elements as Sticky, Bubble, Dash, and Tool. jsApplication includes CSS definitions helping to define a customizable, but consistent looking theme for an application. In order to use jsApplication absolutely no server-side script or calculation is necessary, i.e. no server at all is necessary. A pure .html file and a browser is sufficient.

* Website: https://jsapplication.gitlab.io/jsapplication/
* Main repository (including Documentation and Examples): https://gitlab.com/jsapplication/jsapplication/ 
